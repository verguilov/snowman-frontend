export const config = {
    toolbar: {
        items: [
            'bold',
            'italic',
            '|',
            'bulletedList',
            'numberedList',
            'indent',
            'outdent',
            '|',
            'heading',
            'blockquote',
            '|',
            'imageInsert',
            'undo',
            'redo'
        ]
    }
}