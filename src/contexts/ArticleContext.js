import React, {useState, createContext} from 'react';
import axios from 'axios';
export const ArticleContext = createContext();

export const ArticleProvider = (props) => {
    const fetchItems = async () => {
        const result = await axios('https://api.thesnowman.org/api/v1/articles');
        
        const items = await result.data;
                
        setItems(items);
    }
    
    const [items, setItems] = useState(() => {
        fetchItems()
    });
    
    return(
        <ArticleContext.Provider value={[items, setItems]}>
            {props.children}
        </ArticleContext.Provider>
    );
}