import React, {useState, createContext} from 'react';

export const ContentContext = createContext();

export const TestProvider = (props) => {
    
    let [article, setArticle] = useState('');
    
    return(
        <ContentContext.Provider value={[article, setArticle]}>
            {props.children}
        </ContentContext.Provider>
    );
}