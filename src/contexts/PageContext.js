import React, {useState, createContext} from 'react';

export const PageContext = createContext();

export const PageProvider = (props) => {
    
    let [counter, setCounter] = useState(0);

    return(
        <PageContext.Provider value={[counter, setCounter]}>
            {props.children}
        </PageContext.Provider>
    );
}