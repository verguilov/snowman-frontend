import { createContext, useEffect, useState } from 'react';

export const AuthContext = createContext();

export const AuthProvider = (props) => {

    const [token, setToken] = useState();
    
    useEffect(() => {
        if(token) {
            localStorage.removeItem('snowmanToken');
            localStorage.setItem('snowmanToken', token);
        }
    })

    return(
        <AuthContext.Provider value={[token, setToken]}>
            {props.children}
        </AuthContext.Provider>
    );
}

export default AuthContext;
