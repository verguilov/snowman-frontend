import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({component: Component, ...rest}) => {

    const loggedInUser = () => {
        return localStorage.getItem('snowmanToken') ? true : false;
    }

    return (
        <Route {...rest} render={props => (
            loggedInUser() ?
                <Component {...props} />
            : <Redirect to="/login" />
        )} />
    );
};

export default PrivateRoute;