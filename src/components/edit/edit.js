import React, { useContext, useEffect, useState } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ContentContext } from '../../contexts/ContentContext';
import {Helmet} from "react-helmet";
import axios from 'axios';

function Edit({ match }) {

    const fetchItem = async () => {
        
        const result = await axios(`https://api.thesnowman.org/api/v1/articles/${match.params.id}`);

        const item = await result.data;
        
        setItem(item);
    }

    const [item, setItem] = useState(() => {
        fetchItem()
    });

    const [title, setTitle] = useState('');

    const [subtitle, setSubtitle] = useState('');

    const [content, setContent] = useState('');

    const [url, setUrl] = useState('');

    useEffect(() => {
        if(item) {
            setTitle(item.title);
            setSubtitle(item.subTitle);
            setContent(item.content);
            setUrl(item.url);        }
    }, [item, setItem])

    let [article, setArticle] = useContext(ContentContext);
    
    const getTitle = e => {
        setTitle(e.target.value);
    }
    
    const getSubtitle = e => {
        setSubtitle(e.target.value);
    }

    const getUrl = e => {
        setUrl(e.target.value);
    }

    const sendArticle = e => {
        e.preventDefault();

        if(title && subtitle && content && url) {
            article = {
                id: item.id,
                title: title,
                subtitle: subtitle,
                content: content,
                url: url
            }
            setArticle(article)
        } else {
            alert('Please fill out all the fields!')
        }
    };

    return <div className='draftWrap'>
                <Helmet>
                    <title>The SNOWman Article Draft</title>
                    <meta name="description" content="Article draft page" />
                </Helmet>
                <form>
                    <textarea
                        placeholder="Title"
                        value={title}
                        required
                        onChange={getTitle}/>
                    <textarea
                        placeholder="Subtitle"
                        value={subtitle}
                        required
                        onChange={getSubtitle}/>
                    <textarea
                        placeholder="Image URL"
                        value={url}
                        required
                        onChange={getUrl}/>
                    <CKEditor
                        classname="draft"
                        required
                        editor={ClassicEditor}
                        data={content}
                        onChange={(e, editor) => {
                            setContent(editor.getData());
                        }}/>
                    <div style={{margin: "1rem"}}>
                            <button onClick={sendArticle}>Submit</button>
                    </div>
                </form>
            </div>
}

export default Edit;