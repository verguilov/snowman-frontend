import { useContext, useEffect } from "react";
import { Link } from 'react-router-dom';
import { ArticleContext } from "../../contexts/ArticleContext";
import InfiniteScroll from 'react-infinite-scroll-component'; 
import { PageContext } from "../../contexts/PageContext";
import {Helmet} from "react-helmet";

function FrontView() {
    const [items, setItems] = useContext(ArticleContext);
    let [counter, setCounter] = useContext(PageContext);

    const incrementPage = () => {
        counter++
        setCounter(counter);
    }

    useEffect(() => {
        setItems(items);
    })
    
    if (items) {

    return  <InfiniteScroll
                style={{overflow: "hidden"}}
                dataLength={items.length}
                next={incrementPage}
                hasMore={true}>
                <div className="mainSector">
                    <Helmet>
                        <title>The SNOWman Library</title>
                        <meta name="title" content="The SNOWman Library" property="og:title" />
                        <meta name="description" property="og:description" content="A knowledge base project, revolving around Service Now, JavaScript programming, and life and the Universe in general" />
                    </Helmet>
                    {items.map(item => (
                        <Link key={item.id} to={`/articles/${item.id}`}>
                            <div>
                                <div className="imgContainer">
                                    <img alt="" src={item.url} className="artImg"/>
                                    <div className="titleContainer">
                                        <p style={{textAlign: "right",
                                                padding: "0 .5rem 0 0"}}>
                                            {item.timestamp.split('T').join(' ').split('.')[0]}</p>
                                        <h2 style={{margin: "auto 0 1rem 0",
                                                    padding: "0 1.5rem 0 1.5rem",
                                                    textAlign: "left"
                                        }}>{item.title}</h2>
                                        <h4 style={{margin: 0,
                                                    padding: "0 1.5rem .5rem 1.5rem"
                                        }}>{'"' + item.subTitle.split('.')[0] + '..."'}</h4>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    ))}
                </div>
            </InfiniteScroll>
    } else {
        return <div></div>
    }
}


export default FrontView;