import React, { useContext, useState } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ContentContext } from '../../contexts/ContentContext';
import {Helmet} from "react-helmet";
import { config } from '../../configs/editorConfig';


function Draft() {
    ClassicEditor.defaultConfig = config

    

    const [title, setTitle] = useState('')
    const [subtitle, setSubtitle] = useState('');
    const [content, setContent] = useState('');
    const [url, setUrl] = useState('');
    let [article, setArticle] = useContext(ContentContext);
    
    const getTitle = e => {
        setTitle(e.target.value);
    }
    
    const getSubtitle = e => {
        setSubtitle(e.target.value);
    }

    const getUrl = e => {
        setUrl(e.target.value);
    }

    const sendArticle = ((e) => {
        e.preventDefault();

        if(title && subtitle && content && url) {
            article = {
                title: title,
                subtitle: subtitle,
                content: content,
                url: url
            }
            setArticle(article)
        } else {
            alert('Please fill out all the fields!')
        }
    })

    return <div className='draftWrap'>
                <Helmet>
                    <title>The SNOWman Article Draft</title>
                    <meta name="description" content="Article draft page" />
                </Helmet>
                <form onSubmit={sendArticle}>
                    <textarea
                        placeholder="Title"
                        required
                        onChange={getTitle}/>
                    <textarea
                        placeholder="Subtitle"
                        required
                        onChange={getSubtitle}/>
                    <textarea
                        placeholder="Image URL"
                        required
                        onChange={getUrl}/>
                    <CKEditor
                        classname="draft"
                        required
                        editor={ClassicEditor}
                        data={content}
                        onChange={(e, editor) => {
                            setContent(editor.getData());
                        }}/>
                    <div style={{margin: "1rem"}}>
                        <button type="submit">Submit</button>
                    </div>
                </form>
            </div>
}

export default Draft;