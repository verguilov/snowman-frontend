/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import the_snowman from './the_snowman.png'
import logo from '../../logo.svg';
import { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import { ArticleContext } from '../../contexts/ArticleContext'
import axios from 'axios';
import { PageContext } from '../../contexts/PageContext';
import { ContentContext } from '../../contexts/ContentContext';
import AuthContext from '../../contexts/AuthContext';
import jwtDecode from 'jwt-decode';

function Header() {

    const fetchItems = async (title, skip) => {
        if(title) {
            const result = await axios('https://api.thesnowman.org/api/v1/articles?title=' + title);
            
            const arts = await result.data;
                        
            setArts(arts);

        } else if (skip) {
            const result = await axios('https://api.thesnowman.org/api/v1/articles?skip=' + skip);
            
            const arts = await result.data;
                        
            setArts(prevArts => [...prevArts, ...arts]);

        } else {
            const result = await axios('https://api.thesnowman.org/api/v1/articles');
            
            const arts = await result.data;
                        
            setArts(arts);
        }

    }

    const createArticle = async () => {
        try {
            if (article) {
                if(article.id) {
                    await axios.put(`https://api.thesnowman.org/api/v1/articles/edit/${article.id}`,
                {
                    title: article.title,
                    subTitle: article.subtitle,
                    content: article.content,
                    author: loggedInUser,
                    url: article.url
                },
                {
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('snowmanToken')
                    }
                })
                window.location.replace(`/articles/${article.id}`)

                } else {
                    await axios.post('https://api.thesnowman.org/api/v1/articles',
                    {
                        title: article.title,
                        subTitle: article.subtitle,
                        content: article.content,
                        author: loggedInUser,
                        url: article.url
                    },
                    {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('snowmanToken')
                        }
                    })


                    .then((res) => {
                        console.log(res.data.id);
                        window.location.replace(`/articles/${res.data.id}`);
                    }
                        
                    )
                }
            }

        } catch (error) {
            if(error.message.indexOf('401') > -1) {
                alert(error.message);
                localStorage.clear();
                window.location.replace('/login');
            } else {
                alert(error.message);
            }
        }
    }
    
    const [arts, setArts] = useState(() => {
        fetchItems()
    });

    const [loggedInUser, setLoggedInUser] = useState('')
    const [items, setItems] = useContext(ArticleContext);
    const [counter, setCounter] = useContext(PageContext);
    const [article, setArticle] = useContext(ContentContext);
    const [token] = useContext(AuthContext);

    useEffect(() => {
        setTimeout(() => {
            if(localStorage.getItem('snowmanToken')){
                setLoggedInUser(jwtDecode(localStorage.getItem('snowmanToken')).username);
            }
        }, 0)
    }, [token]);

    useEffect(() => {
        createArticle();
        fetchItems();
    }, [article, setArticle]);

    useEffect(() => {
        fetchItems('', counter*20);
    },[counter, setCounter])

    useEffect(() => {
        setItems(arts);
    }, [arts, setArts, setItems]);

    const refreshArticles = e => {
        fetchItems(e.target.value, counter);
        setItems(prevItems => arts);
    };

    const logout = () => {
        localStorage.clear();
        setLoggedInUser(null);
    }

    return  (<span className="headerMain">
                <img src={logo}
                    className="App-logo"
                    alt="logo"/>
                <Link to={'/'}>
                    <img src={the_snowman}
                        alt=""
                        className="snmLogo"/>
                </Link>
                <div className="headerOptions">                    
                    <input type="text" placeholder="Search..." onChange={refreshArticles}></input>    
                    {loggedInUser ? <div className="headOption">
                                        <Link to={'/draft'}>
                                            <li>Create Article</li>
                                        </Link>
                                    </div>
                                    : null}
                    
                    {!loggedInUser ? <div className="headOption">
                                        <Link to={'/login'}>
                                            <li className="headerItem">Login</li>
                                        </Link>
                                    </div> 
                                    :   <div className="headOption">
                                            <Link to={'/'}>
                                                <li className="headerItem" onClick={logout}>Logout {loggedInUser}</li>
                                            </Link>
                                        </div>}
                </div>
                <div className="headOption"></div>
            </span>)
}

export default Header;