import axios from "axios";
import { useState } from "react";
import parse from 'html-react-parser';
import {Helmet} from "react-helmet";
import { Link } from "react-router-dom";
import ReactTooltip from "react-tooltip";

function Article({ match }) {
    
    const fetchItem = async () => {
        
        const result = await axios(`https://api.thesnowman.org/api/v1/articles/${match.params.id}`);

        const item = await result.data;
        
        setItem(item);
    }

    const [item, setItem] = useState(() => {
        fetchItem()
    });

    const deleteArticle = async e => {
        e.preventDefault();

        if (window.confirm('Are you sure you want to delete this article?')) {
            axios.delete(`https://api.thesnowman.org/api/v1/articles/delete/${item.id}`,
                {
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('snowmanToken')
                    }
                }
            ).then(() => {
                window.location.replace('/');
            })            
        }
    }

    if(item) {
    return  <div className="articleMain">                
                <Helmet>
                    <title>The SNOWman - {item.title}</title>
                    <meta name="title" content={item.title} property="og:title"/>
                    <meta name="description" content={item.subTitle} property="og:description" />
                </Helmet>
                <div className="artMainButtons">
                    {
                        localStorage.getItem('snowmanToken') ? 
                            <div>
                                <Link to={`/edit/${item.id}`}>
                                    <button data-tip data-for="editTip">Edit</button>
                                </Link>
                                <ReactTooltip id="editTip" place="top" effect="solid">Edit Article</ReactTooltip>
                                <button data-tip data-for="deleteTip" onClick={deleteArticle}>Delete</button>
                                <ReactTooltip id="deleteTip" place="top" effect="solid">Delete Article</ReactTooltip>

                            </div>
                            :
                            null
                    }
                </div>
                <div className='articleHeading'>
                    <div className="titleBlock">                    
                        <h1>{item.title}</h1>
                        <h5>{item.subTitle}</h5> 
                        <h5
                            style={{margin: "0 0 1rem 0rem",
                                    textAlign: "right"}}
                        >{item.author + ', ' + item.timestamp.split('T').join(' ').split('.')[0]}</h5>                   
                    </div>
                </div>
                <div>{parse(' ' + item.content)}</div>
            </div>
    } else {
        return <div></div>
    }
}

export default Article;