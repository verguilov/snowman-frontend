import axios from "axios";
import { useContext, useState } from "react";
import AuthContext from "../../contexts/AuthContext";
import {Helmet} from "react-helmet";

function Login() {
    
    const [user, setUser] = useState('');
    const [pass, setPass] = useState('');
    let [token, setToken] = useContext(AuthContext);
        
    const getUser = e => {
        setUser(e.target.value)
    };
    
    const getPass = e => {
        setPass(e.target.value)
    };
    
    const sendLogin = async (e) => {
        e.preventDefault()
        try {
            const result = await axios.post('https://api.thesnowman.org/api/v1/session/login',
            {
                username: user,
                password: pass
            })

            token = result.data.authToken;

            setToken(token);
            window.location.replace('/');
            
        } catch (error) {
            alert('Incorrect username or password!');
        }
    }
    
    return <div>
                <Helmet>
                    <title>The SNOWman Login</title>
                    <meta name="description" content="The SNOWman login page" />
                </Helmet>
                <form onSubmit={sendLogin}>
                    <input type="text"
                    placeholder="Username/Email"
                    required
                    onChange={getUser}></input>
                    <input type="password"
                    placeholder="Password"
                    required
                    onChange={getPass}></input>
                    <button type="submit">Login</button>
                </form>
            </div>
}

export default Login;