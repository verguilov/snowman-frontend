import './App.css';
import Header from './components/header/header';
import FrontView from './components/frontView/frontView';
import Article from './components/article/article'
import Draft from './components/draft/draft';
import Login from './components/login/login';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ArticleProvider } from './contexts/ArticleContext'
import { PageProvider } from './contexts/PageContext';
import { TestProvider } from './contexts/ContentContext';
import { AuthProvider } from './contexts/AuthContext';
import PrivateRoute from './guards/PrivateRoute';
import Edit from './components/edit/edit';

function App() {
  return (
    <ArticleProvider>
    <PageProvider>
    <TestProvider>
    <AuthProvider>
      <Router>
        <div className="App">
            <Header></Header>
          <Route path="/" component={FrontView} exact/>
          <Route path="/articles/:id" component={Article}/>
          <Route path="/login" component={Login}/>
          <PrivateRoute path="/draft" component={Draft}/>
          <PrivateRoute path="/edit/:id" component={Edit}/>
        </div>
      </Router>
    </AuthProvider>
    </TestProvider>
    </PageProvider>
    </ArticleProvider>
  );
}

export default App;
